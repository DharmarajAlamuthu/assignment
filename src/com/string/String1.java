package com.string;

public class String1 {
	public static void main(String[] args) {

		String s1 = "Welcome to GlobalLogic  ";
		System.out.println(s1);

		System.out.println(s1.length());
		boolean b = s1.endsWith(" globallogic");
		System.out.println(b);
		boolean c = s1.contains("Welcome");
		System.out.println(c);

		boolean d = s1.contains("banglore");
		System.out.println(d);
		String[] e = s1.split(" ");
		System.out.println("======================");
		for (String string : e) {
			System.out.println(string);

		}
		String upperCase = s1.toUpperCase();
		System.out.println(upperCase);

		String lowerCase = s1.toLowerCase();
		System.out.println(lowerCase);

		String substring = s1.substring(3);
		System.out.println(substring);
		int indexOf = s1.indexOf("c");
		System.out.println(indexOf);
		int lastIndexOf = s1.lastIndexOf("c");
		System.out.println(lastIndexOf);
		String replace = s1.replace("Welcome", "Hello");
		System.out.println(replace);
		System.out.println(System.identityHashCode(s1));
		String s2 = new String("Hello world");
		System.out.println(System.identityHashCode(s2));


	}
}